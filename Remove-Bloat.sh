#!/bin/bash
#Feel Free to add this distros own backgrounds into /usr/share/gnome-background-properties
echo "Removing useless packages......."
apt autoremove --purge gimp -y
apt autoremove --purge evolution -y
apt autoremove --purge cheese -y
apt autoremove --purge goldendict -y
apt autoremove --purge transmission-gtk -y
apt autoremove --purge rhythmbox -y
apt autoremove --purge thunderbird -y
apt autoremove --purge xterm -y
apt autoremove --purge uxterm -y
apt autoremove --purge fcitx -y
apt autoremove --purge gnome-games -y
apt autoremove --purge shotwell -y
apt autoremove --purge gnome-media -y
apt autoremove --purge hdate-applet -y
apt autoremove --purge kasumi -y
apt autoremove --purge gnome-sound-recorder -y
apt autoremove --purge gnome-music -y
apt autoremove --purge gnome-todo -y
apt autoremove --purge gnome-weather -y
apt autoremove --purge gnome-maps -y
apt autoremove --purge gnome-clocks -y
apt autoremove --purge gnome-calendar -y
apt autoremove --purge gnome-documents -y
apt autoremove --purge totem-plugins -y
apt autoremove --purge totem -y
apt autoremove --purge mlterm -y
apt autoremove --purge xiterm+thai -y
apt autoremove --purge mozc-utils-gui -y
apt autoremove --purge mlterm-common -y
apt autoremove --purge malcontent -y
apt autoremove --purge fcitx-bin -y
apt autoremove --purge fcitx-data -y
apt autoremove --purge fcitx-modules -y
apt autoremove --purge fcitx-config-gtk -y
apt autoremove --purge kde-config-fcitx -y
apt autoremove --purge fcitx-frontend-all -y
apt autoremove --purge fcitx-frontend-fbterm -y
apt autoremove --purge fcitx-ui-classic -y
apt autoremove --purge fcitx-ui-light -y
apt autoremove --purge im-config -y
apt autoremove --purge fcitx-m17n -y
apt autoremove --purge fcitx-tools -y
apt autoremove --purge fcitx-frontend-all -y
apt autoremove --purge fcitx-table-wbpy -y
apt autoremove --purge fcitx-table-cangjie -y
apt autoremove --purge fctix5-chinese-addons -y
apt autoremove --purge fctix5 -y
apt autoremove --purge fctix5-chewing -y
apt autoremove --purge fctix5-chinese-addons-bin -y
apt autoremove --purge fctix5-chinese-addons-data -y
apt autoremove --purge fctix5-chinese-addons -y
apt autoremove --purge fctix5-tables -y
apt autoremove --purge yelp -y
apt autoremove --purge gnome-firmware -y
apt autoremove --purge anthy -y
apt autoremove --purge evince -y
apt autoremove --purge uim -y
apt autoremove --purge mozc-data -y
apt autoremove --purge debian-reference-it -y
apt autoremove --purge gnome-contacts -y
apt autoremove --purge plymouth plymouth-themes -y
apt autoremove --purge task-ukrainian-desktop -y
apt autoremove --purge task-japanese-gnome-desktop -y
apt autoremove --purge task-japanese-desktop -y
apt autoremove --purge task-hindi-desktop -y
apt autoremove --purge task-hebrew-gnome-desktop -y
apt autoremove --purge task-hebrew-desktop -y
apt autoremove --purge task-korean-desktop -y
apt autoremove --purge task-korean-gnome-desktop -y
apt autoremove --purge task-kurdish-desktop -y
apt autoremove --purge task-french-desktop -y
apt autoremove --purge task-chinese-s-desktop -y
apt autoremove --purge task-chinese-t-desktop -y
apt autoremove --purge qemu-utils -y
apt autoremove --purge qemu-system-x86 -y
apt autoremove --purge qemu-system-gui -y
apt autoremove --purge qemu-system-data -y
apt autoremove --purge qemu-system-common -y
apt autoremove --purge vim-common -y
apt autoremove --purge fcitx5-chewing -y
apt autoremove --purge fcitx5-chinese-addons-bin -y
apt autoremove --purge fcitx5-chinese-addons-data -y
apt autoremove --purge fcitx5-chinese-addons -y
apt autoremove --purge fcitx5-config-qt -y
apt autoremove --purge fcitx5-data -y
apt autoremove --purge fcitx5-frontend-gtk3 -y
apt autoremove --purge fcitx5-frontend-qt5 -y
apt autoremove --purge fcitx5-module-chttrans -y
apt autoremove --purge fcitx5-module-cloudpinyin -y
apt autoremove --purge fcitx5-module-emoji -y
apt autoremove --purge fcitx5-module-fullwidth -y
apt autoremove --purge fcitx5-module-pinyinhelper -y
apt autoremove --purge fcitx5-module-punctuation -y
apt autoremove --purge fcitx5-module-quickphrase -y
apt autoremove --purge fcitx5-module-wayland -y
apt autoremove --purge fcitx5-module-xorg -y
apt autoremove --purge fcitx5-modules -y
apt autoremove --purge fcitx5-pinyin -y
apt autoremove --purge fcitx5-table -y
apt autoremove --purge apache2-bin -y
apt autoremove --purge zathura -y
apt autoremove --purge smartmontools -y
apt autoremove --purge task-esperanto-desktop -y
apt autoremove --purge task-finnish-desktop -y
apt autoremove --purge task-galician-desktop -y
apt autoremove --purge task-georgian-desktop -y
apt autoremove --purge task-german-desktop -y
apt autoremove --purge task-greek-desktop -y
apt autoremove --purge task-gujarati-desktop -y
apt autoremove --purge task-hungarian-desktop -y
apt autoremove --purge task-icelandic-desktop -y
apt autoremove --purge task-indonesian-desktop -y
apt autoremove --purge task-irish-desktop -y
apt autoremove --purge task-italian-desktop -y
apt autoremove --purge task-kannada-desktop -y
apt autoremove --purge task-kazakh-desktop -y
apt autoremove --purge task-khmer-desktop -y
apt autoremove --purge task-latvian-desktop -y
apt autoremove --purge task-lithuanian-desktop -y
apt autoremove --purge task-macedonian-desktop -y
apt autoremove --purge task-malayalam-desktop -y
apt autoremove --purge task-marathi-desktop -y
apt autoremove --purge task-nepali-desktop -y
apt autoremove --purge task-northern-sami-desktop -y
apt autoremove --purge task-norwegian-desktop -y
apt autoremove --purge task-polish-desktop -y
apt autoremove --purge task-portuguese-desktop -y
apt autoremove --purge task-punjabi-desktop -y
apt autoremove --purge task-romanian-desktop -y
apt autoremove --purge task-russian-desktop -y
apt autoremove --purge task-serbian-desktop -y
apt autoremove --purge task-sinhala-desktop -y
apt autoremove --purge task-slovak-desktop -y
apt autoremove --purge task-slovenian-desktop -y
apt autoremove --purge task-spanish-desktop -y
apt autoremove --purge task-swedish-desktop -y
apt autoremove --purge task-tamil-desktop -y
apt autoremove --purge task-tamil-gnome-desktop -y
apt autoremove --purge task-telugu-desktop -y
apt autoremove --purge task-telugu-gnome-desktop -y
apt autoremove --purge task-thai-desktop -y
apt autoremove --purge task-thai-gnome-desktop -y
apt autoremove --purge task-turkish-desktop -y
apt autoremove --purge task-uyghur-desktop -y
apt autoremove --purge task-vietnamese-desktop -y
apt autoremove --purge task-welsh-desktop -y
apt autoremove --purge task-xhosa-desktop -y
apt autoremove --purge task-asturian-desktop -y
apt autoremove --purge task-basque-desktop -y
apt autoremove --purge task-belarusian-desktop -y
apt autoremove --purge task-bengali-desktop -y
apt autoremove --purge task-bosnian-desktop -y
apt autoremove --purge task-brazilian-portuguese-desktop -y
apt autoremove --purge task-bulgarian-desktop -y
apt autoremove --purge task-catalan-desktop -y
apt autoremove --purge task-croatian-desktop -y
apt autoremove --purge task-cyrillic-desktop -y
apt autoremove --purge task-czech-desktop -y
apt autoremove --purge task-danish-desktop -y
apt autoremove --purge task-dutch-desktop -y
apt autoremove --purge task-dzongkha-desktop -y
apt autoremove --purge task-albanian-desktop -y
apt autoremove --purge task-amharic-desktop -y
apt autoremove --purge kactivitymanagerd -y
apt autoremove --purge kactivities-bin -y
apt autoremove --purge aspell -y
apt autoremove --purge arm-trusted-firmware-tools -y
apt autoremove --purge systemsettings -y
apt autoremove --purge kde5 -y
apt autoremove --purge kde -y
apt autoremove --purge zenity -y
apt autoremove --purge xorg-docs-core -y
apt autoremove --purge xauth -y
apt autoremove --purge intel-microcode -y
apt autoremove --purge amd64-microcode -y
apt autoremove --purge synaptic -y
apt autoremove --purge wswedish -y
apt autoremove --purge wspanish -y
apt autoremove --purge wportugues -y
apt autoremove --purge wpolish -y
apt autoremove --purge wnorwegian -y
apt autoremove --purge wgerman -y
apt autoremove --purge witalian -y
apt autoremove --purge wfrench -y
apt autoremove --purge wdutch -y
apt autoremove --purge wdanish -y
apt autoremove --purge wcatalan -y
apt autoremove --purge wbulgarian -y
apt autoremove --purge wbrazilian -y
apt autoremove --purge wngerman -y
apt autoremove --purge tpm-udev -y
apt autoremove --purge hunspell -y
apt autoremove --purge mysql-common -y
apt autoremove --purge myspell-eo -y
apt autoremove --purge myspell-es -y
apt autoremove --purge myspell-et -y
apt autoremove --purge myspell-fa -y
apt autoremove --purge myspell-ga -y
apt autoremove --purge myspell-he -y
apt autoremove --purge myspell-nb -y
apt autoremove --purge myspell-nn -y
apt autoremove --purge myspell-sk -y
apt autoremove --purge myspell-sq -y
apt autoremove --purge myspell-uk -y
apt autoremove --purge mobile-broadband-provider-info -y
apt autoremove --purge gnome-terminal -y
apt autoremove --purge seahorse -y
apt autoremove --purge hunspell-be -y
apt autoremove --purge hunspell-bg -y
apt autoremove --purge hunspell-bs -y
apt autoremove --purge hunspell-ca -y
apt autoremove --purge hunspell-cs -y
apt autoremove --purge hunspell-da -y
apt autoremove --purge hunspell-de-at -y
apt autoremove --purge hunspell-de-ch -y
apt autoremove --purge hunspell-de-de -y
apt autoremove --purge hunspell-el -y
apt autoremove --purge hunspell-en-gb -y
apt autoremove --purge hunspell-eu -y
apt autoremove --purge hunspell-fr-classical -y
apt autoremove --purge hunspell-gl -y
apt autoremove --purge hunspell-gu -y
apt autoremove --purge hunspell-hi -y
apt autoremove --purge hunspell-hr -y
apt autoremove --purge hunspell-hu -y
apt autoremove --purge hunspell-id -y
apt autoremove --purge hunspell-is -y
apt autoremove --purge hunspell-it -y
apt autoremove --purge hunspell-kk -y
apt autoremove --purge hunspell-kmr -y
apt autoremove --purge hunspell-ko -y
apt autoremove --purge hunspell-lt -y
apt autoremove --purge hunspell-lv -y
apt autoremove --purge hunspell-ne -y
apt autoremove --purge hunspell-nl -y
apt autoremove --purge hunspell-pl -y
apt autoremove --purge hunspell-pt-br -y
apt autoremove --purge hunspell-pt-pt -y
apt autoremove --purge hunspell-ro -y
apt autoremove --purge hunspell-ru -y
apt autoremove --purge hunspell-si -y
apt autoremove --purge hunspell-sl -y
apt autoremove --purge hunspell-sr -y
apt autoremove --purge hunspell-sv -y
apt autoremove --purge hunspell-te -y
apt autoremove --purge hunspell-th -y
apt autoremove --purge hunspell-vi -y
apt autoremove --purge debian-installer -y
apt autoremove --purge akregator -y
apt autoremove --purge raspi-firmware -y
apt autoremove --purge abrt-desktop -y
apt autoremove --purge dahdi-firmware-nonfree -y
apt autoremove --purge atmel-firmware uuid-runtime firmware-samsung firmware-siano firmware-nvidia-tesla-gsp firmware-ast firmware-ivtv firmware-bnx2 firmware-bnx2x firmware-cavium firmware-myricom firmware-netxen firmware-netronome -y
# Wayland FTW
apt autoremove --purge xorg xserver-xorg-core xinit -y
echo "WHAT THE FUCK DEBIAN DEVS??????"
apt autoremove --purge unattended-upgrades -y
echo "Debranding"
rm -rf /usr/share/backgrounds
rm -rf /usr/share/pixmaps/backgrounds
rm -rf /usr/local/share/gnome-shell/extensions/extension-uuid/ 
rm -rf ~/.local/share/gnome-shell/extensions/
rm -rf /usr/share/sddm/themes/debian-theme
rm -rf /usr/share/sddm/themes/debian-maui
rm -rf /usr/share/wallpapers/DebianTheme
rm -rf /usr/share/wallpapers/FuturePrototype
rm -rf /usr/share/wallpapers/FuturePrototypeWithLogo
rm -rf /usr/share/wallpapers/Joy
rm -rf /usr/share/wallpapers/JoyInksplat
rm -rf /usr/share/wallpapers/Lines
rm -rf /usr/share/wallpapers/LinesLockScreen
rm -rf /usr/share/wallpapers/Next
rm -rf /usr/share/wallpapers/SoftWaves
rm -rf /usr/share/wallpapers/SoftWavesLockScreen
rm -rf /usr/share/wallpapers/SpaceFun
rm -rf /usr/share/wallpapers/homeworld
rm -rf /usr/share/wallpapers/homeworld_wallpaper
rm -rf /usr/share/wallpapers/moonlight
rm -rf /usr/share/wallpapers/MoonlightLockScreen
rm -rf /usr/share/wallpapers/JoyLockScreen
rm -rf /usr/bin/debian-reference /usr/share/debian-reference /usr/share/man/man1/debian-reference.1.gz
rm -rf /usr/share/icons/vendor/scalable/emblems
rm -rf /usr/share/backgrounds/gnome
rm -rf /usr/share/images/desktop-base/
rm -rf /usr/share/images/vendor-logos/
rm -rf /usr/share/wallpapers/
rm -rf /usr/share/backgrounds/gnome
rm -rf /usr/share/gnome-background-properties
rm -rf /var/lib/AccountService/icons/
rm -rf /usr/share/pixmaps/faces
rm -rf /usr/share/wallpapers/emerald
rm -rf /usr/share/wallpapers/emerald_wallpaper
rm -rf /etc/skel/.face
rm -rf /usr/share/applications/software-properties-gtk.desktop
rm -rf /etc/dpkg/origins/debian
rm -rf /usr/share/desktop-base/debian-logos
rm -rf /usr/share/desktop-base/debian-homepage.desktop
rm -rf /usr/share/desktop-base/debian-reference.desktop
rm -rf /usr/share/desktop-base/debian-security.desktop
rm -rf /usr/share/desktop-base/emerald-theme
rm -rf /usr/share/desktop-base/homeworld-theme
rm -rf /usr/share/desktop-base/grub_background.sh
rm -rf /usr/share/desktop-base/futureprototype-theme
rm -rf /usr/share/desktop-base/homeworld-theme
rm -rf /usr/share/desktop-base/joy-inksplat-theme
rm -rf /usr/share/desktop-base/joy-theme
rm -rf /usr/share/desktop-base/lines-theme
rm -rf /usr/share/desktop-base/moonlight-theme
rm -rf /usr/share/desktop-base/softwaves-theme
rm -rf /usr/share/desktop-base/spacefun-theme
rm -rf /usr/share/icons/vendor
rm -rf /usr/share/icons/desktop-base
echo "I am saint ignucius of the church of emacs, I bless this distro."
rm -rf /usr/bin/vi /usr/share/man/man1/vi.1.gz
rm -rf /usr/bin/vim.tiny /etc/vim /usr/share/vim/ /usr/share/man/man1/vim.1.gz
echo "xfce"
rm -rf /usr/share/images/desktop-base/