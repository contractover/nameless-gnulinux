#!/bin/bash
echo "Name[ar]=محار" | tee -a /usr/share/applications/clamtk.desktop
rm -rf /usr/share/applications/codium.desktop
touch /usr/share/applications/codium.desktop
echo "[Desktop Entry]
Name=VSCodium
Name[ar]=كوديوم
Comment=Code Editing. Redefined.
GenericName=Text Editor
Exec=/usr/share/codium/codium --unity-launch %F
Icon=vscodium
Type=Application
StartupNotify=false
StartupWMClass=VSCodium
Categories=TextEditor;Development;IDE;
MimeType=text/plain;inode/directory;application/x-codium-workspace;
Actions=new-empty-window;
Keywords=vscode;

[Desktop Action new-empty-window]
Name=New Empty Window
Exec=/usr/share/codium/codium --new-window %F
Icon=vscodium" | tee -a /usr/share/applications/codium.desktop

rm -rf /usr/share/applications/com.github.maoschanz.drawing.desktop
touch /usr/share/applications/com.github.maoschanz.drawing.desktop
echo "[Desktop Entry]
Name[ar]=رسم
Name=Drawing
Comment=Simple drawing utility
Exec=drawing %U
# Icon name, do not translate
Icon=com.github.maoschanz.drawing
Terminal=false
Type=Application
StartupNotify=true
# This is a list of keywords, you can add any keyword that would be useful to
# find the app in your language. "Paint" should stay listed in its untranslated
# version too, so people looking for a MS Paint equivalent will find the app.
# Don't forget the semicolons.
Keywords=Paint;Sketch;Pencil;
Categories=Graphics;GNOME;GTK;
MimeType=image/png;image/bmp;image/jpeg;
Actions=new-window;new-tab;edit-clipboard;
# Translators: Do NOT translate or transliterate this text
X-Purism-FormFactor=Workstation;Mobile;

[Desktop Action new-window]
Name=New Window
Exec=drawing --new-window

[Desktop Action new-tab]
Name=New Image
Exec=drawing --new-tab

[Desktop Action edit-clipboard]
Name=Edit Image in Clipboard
Exec=drawing --edit-clipboard" | tee -a /usr/share/applications/com.github.maoschanz.drawing.desktop

rm -rf /usr/share/applications/firefox-esr.desktop
touch /usr/share/applications/firefox-esr.desktop
echo "[Desktop Entry]
Name=Firefox ESR
Name[ar]=فايرفوكس
Comment=Browse the World Wide Web
GenericName=Web Browser
X-GNOME-FullName=Firefox ESR Web Browser
Exec=/usr/lib/firefox-esr/firefox-esr %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=firefox-esr
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupWMClass=Firefox-esr
StartupNotify=true" | tee -a /usr/share/applications/firefox-esr.desktop

rm -rf /usr/share/applications/libreoffice-calc.desktop
touch /usr/share/applications/libreoffice-calc.desktop
echo "
[Desktop Entry]
Version=1.0
Terminal=false
Icon=libreoffice-calc
Type=Application
Categories=Office;Spreadsheet;
Exec=libreoffice --calc %U
MimeType=application/vnd.oasis.opendocument.spreadsheet;application/vnd.oasis.opendocument.spreadsheet-template;application/vnd.sun.xml.calc;application/vnd.sun.xml.calc.template;application/msexcel;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.ms-excel.sheet.macroEnabled.12;application/vnd.openxmlformats-officedocument.spreadsheetml.template;application/vnd.ms-excel.template.macroEnabled.12;application/vnd.ms-excel.sheet.binary.macroEnabled.12;text/csv;application/x-dbf;text/spreadsheet;application/csv;application/excel;application/tab-separated-values;application/vnd.lotus-1-2-3;application/vnd.oasis.opendocument.chart;application/vnd.oasis.opendocument.chart-template;application/x-dbase;application/x-dos_ms_excel;application/x-excel;application/x-msexcel;application/x-ms-excel;application/x-quattropro;application/x-123;text/comma-separated-values;text/tab-separated-values;text/x-comma-separated-values;text/x-csv;application/vnd.oasis.opendocument.spreadsheet-flat-xml;application/vnd.ms-works;application/clarisworks;application/x-iwork-numbers-sffnumbers;application/vnd.apple.numbers;application/x-starcalc;
Name=LibreOffice Calc
Name[ar]=ليبريه اوفيس كالك
GenericName[ar]=جدول مُمتد
Comment[ar]=Perform calculations, analyze information and manage lists in spreadsheets.
Comment=Perform calculations, analyze information and manage lists in spreadsheets.
StartupNotify=true
X-GIO-NoFuse=true
Keywords=Accounting;Stats;OpenDocument Spreadsheet;Chart;Microsoft Excel;Microsoft Works;OpenOffice Calc;ods;xls;xlsx;
InitialPreference=5
StartupWMClass=libreoffice-calc
X-KDE-Protocols=file,http,ftp,webdav,webdavs

Actions=NewDocument;
[Desktop Action NewDocument]
Name=New Spreadsheet
Name[ar]=جدول مُمتد جديد
Icon=document-new
Exec=libreoffice --calc" | tee -a /usr/share/applications/libreoffice-calc.desktop

rm -rf /usr/share/applications/libreoffice-draw.desktop
touch /usr/share/applications/libreoffice-draw.desktop
echo "[Desktop Entry]
Version=1.0
Terminal=false
Icon=libreoffice-draw
Type=Application
Categories=Office;FlowChart;Graphics;2DGraphics;VectorGraphics;
Exec=libreoffice --draw %U
MimeType=application/vnd.oasis.opendocument.graphics;application/vnd.oasis.opendocument.graphics-flat-xml;application/vnd.oasis.opendocument.graphics-template;application/vnd.sun.xml.draw;application/vnd.sun.xml.draw.template;application/vnd.visio;application/x-wpg;application/vnd.corel-draw;application/vnd.ms-publisher;image/x-freehand;application/clarisworks;application/x-pagemaker;application/pdf;application/x-stardraw;image/x-emf;image/x-wmf;
Name=LibreOffice Draw
Name[ar]=ليبريه اوفيس رسم
GenericName=Drawing Program
GenericName[ar]=برنامج الرسم
Comment=Create and edit drawings, flow charts and logos.
StartupNotify=true
X-GIO-NoFuse=true
Keywords=Vector;Schema;Diagram;Layout;OpenDocument Graphics;Microsoft Publisher;Microsoft Visio;Corel Draw;cdr;odg;svg;pdf;vsd;
InitialPreference=5
StartupWMClass=libreoffice-draw
X-KDE-Protocols=file,http,ftp,webdav,webdavs

Actions=NewDocument;
[Desktop Action NewDocument]
Name=New Drawing
Name[ar]=رسم جديد
Icon=document-new
Exec=libreoffice --draw" | tee -a /usr/share/applications/libreoffice-draw.desktop

rm -rf /usr/share/applications/libreoffice-impress.desktop
touch /usr/share/applications/libreoffice-impress.desktop
echo "[Desktop Entry]
Version=1.0
Terminal=false
Icon=libreoffice-impress
Type=Application
Categories=Office;Presentation;
Exec=libreoffice --impress %U
MimeType=application/vnd.oasis.opendocument.presentation;application/vnd.oasis.opendocument.presentation-template;application/vnd.sun.xml.impress;application/vnd.sun.xml.impress.template;application/mspowerpoint;application/vnd.ms-powerpoint;application/vnd.openxmlformats-officedocument.presentationml.presentation;application/vnd.ms-powerpoint.presentation.macroEnabled.12;application/vnd.openxmlformats-officedocument.presentationml.template;application/vnd.ms-powerpoint.template.macroEnabled.12;application/vnd.openxmlformats-officedocument.presentationml.slide;application/vnd.openxmlformats-officedocument.presentationml.slideshow;application/vnd.ms-powerpoint.slideshow.macroEnabled.12;application/vnd.oasis.opendocument.presentation-flat-xml;application/x-iwork-keynote-sffkey;application/vnd.apple.keynote;
Name=LibreOffice Impress
Name[ar]=ليبريه اوفيس امبرس
GenericName[ar]=عرض تقديمي
Comment=Create and edit presentations for slideshows, meetings and Web pages.
Comment[ar]=Create and edit presentations for slideshows, meetings and Web pages.
StartupNotify=true
X-GIO-NoFuse=true
Keywords=Slideshow;Slides;OpenDocument Presentation;Microsoft PowerPoint;Microsoft Works;OpenOffice Impress;odp;ppt;pptx;
InitialPreference=5
StartupWMClass=libreoffice-impress
X-KDE-Protocols=file,http,ftp,webdav,webdavs

Actions=NewDocument;
[Desktop Action NewDocument]
Name=New Presentation
Name[ar]=عرض تقديمي جديد
Icon=document-new
Exec=libreoffice --impress" | tee -a /usr/share/applications/libreoffice-impress.desktop

rm -rf /usr/share/applications/libreoffice-startcenter.desktop
touch /usr/share/applications/libreoffice-startcenter.desktop
echo "[Desktop Entry]
Version=1.0
Terminal=false
NoDisplay=false
Icon=libreoffice-startcenter
Type=Application
Categories=Office;X-Red-Hat-Base;X-SuSE-Core-Office;
Exec=libreoffice %U
MimeType=application/vnd.openofficeorg.extension;x-scheme-handler/vnd.libreoffice.cmis;x-scheme-handler/vnd.sun.star.webdav;x-scheme-handler/vnd.sun.star.webdavs;x-scheme-handler/vnd.libreoffice.command;x-scheme-handler/ms-word;x-scheme-handler/ms-powerpoint;x-scheme-handler/ms-excel;x-scheme-handler/ms-visio;x-scheme-handler/ms-access;
Name=LibreOffice
Name[ar]=ليبريه اوفيس
GenericName=Office
GenericName[ar]=مكتب
Comment=Launch applications to create text documents, spreadsheets, presentations, drawings, formulas, and databases, or open recently used documents.
Comment[ar]=Launch applications to create text documents, spreadsheets, presentations, drawings, formulas, and databases, or open recently used documents.
StartupNotify=true
X-GIO-NoFuse=true
StartupWMClass=libreoffice-startcenter
X-KDE-Protocols=file,http,ftp,webdav,webdavs
X-AppStream-Ignore=True

##Define Actions
Actions=Writer;Calc;Impress;Draw;Base;Math;

[Desktop Action Writer]
Name=Writer
Exec=libreoffice --writer

[Desktop Action Calc]
Name=Calc
Exec=libreoffice --calc

[Desktop Action Impress]
Name=Impress
Exec=libreoffice --impress

[Desktop Action Draw]
Name=Draw
Exec=libreoffice --draw

[Desktop Action Base]
Name=Base
Exec=libreoffice --base

[Desktop Action Math]
Name=Math
Exec=libreoffice --math

##End of actions menu" | tee -a /usr/share/applications/libreoffice-startcenter.desktop

rm -rf /usr/share/applications/libreoffice-writer.desktop
touch /usr/share/applications/libreoffice-writer.desktop
echo "[Desktop Entry]
Version=1.0
Terminal=false
Icon=libreoffice-writer
Type=Application
Categories=Office;WordProcessor;
Exec=libreoffice --writer %U
MimeType=application/vnd.oasis.opendocument.text;application/vnd.oasis.opendocument.text-template;application/vnd.oasis.opendocument.text-web;application/vnd.oasis.opendocument.text-master;application/vnd.oasis.opendocument.text-master-template;application/vnd.sun.xml.writer;application/vnd.sun.xml.writer.template;application/vnd.sun.xml.writer.global;application/msword;application/vnd.ms-word;application/x-doc;application/x-hwp;application/rtf;text/rtf;application/vnd.wordperfect;application/wordperfect;application/vnd.lotus-wordpro;application/vnd.openxmlformats-officedocument.wordprocessingml.document;application/vnd.ms-word.document.macroEnabled.12;application/vnd.openxmlformats-officedocument.wordprocessingml.template;application/vnd.ms-word.template.macroEnabled.12;application/vnd.ms-works;application/vnd.stardivision.writer-global;application/x-extension-txt;application/x-t602;text/plain;application/vnd.oasis.opendocument.text-flat-xml;application/x-fictionbook+xml;application/macwriteii;application/x-aportisdoc;application/prs.plucker;application/vnd.palm;application/clarisworks;application/x-sony-bbeb;application/x-abiword;application/x-iwork-pages-sffpages;application/vnd.apple.pages;application/x-mswrite;application/x-starwriter;
Name=LibreOffice Writer
Name[ar]=ليبريه اوفيس كاتب
GenericName=Word Processor
GenericName[ar]=معالج المستندات
GenericName[zu]=Word Processor
Comment=Create and edit text and graphics in letters, reports, documents and Web pages.
Comment[ar]=Create and edit text and graphics in letters, reports, documents and Web pages.
StartupNotify=true
X-GIO-NoFuse=true
Keywords=Text;Letter;Fax;Document;OpenDocument Text;Microsoft Word;Microsoft Works;Lotus WordPro;OpenOffice Writer;CV;odt;doc;docx;rtf;
InitialPreference=5
StartupWMClass=libreoffice-writer
X-KDE-Protocols=file,http,ftp,webdav,webdavs

Actions=NewDocument;
[Desktop Action NewDocument]
Name=New Document
Name[ar]=مستند جديد
Icon=document-new
Exec=libreoffice --writer" | tee -a /usr/share/applications/libreoffice-writer.desktop

rm -rf /usr/share/applications/org.gnome.Console.desktop
touch /usr/share/applications/org.gnome.Console.desktop
echo "[Desktop Entry]
Name[ar]=وحدة التحكم
Name=Console
Exec=kgx
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=org.gnome.Console
# Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
Keywords=command;prompt;cmd;commandline;run;shell;terminal;kgx;kings cross;
Terminal=false
Type=Application
Categories=System;TerminalEmulator;X-GNOME-Utilities;GTK;GNOME;
StartupNotify=true
DBusActivatable=true
X-GNOME-UsesNotifications=true
Actions=new-window;new-tab;

[Desktop Action new-window]
Exec=kgx
Name=New Window
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=window-new

[Desktop Action new-tab]
Name=New Tab
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=tab-new" | tee -a /usr/share/applications/org.gnome.Console.desktop

rm -rf /usr/share/applications/org.gnome.Extensions.desktop
touch /usr/share/applications/org.gnome.Extensions.desktop
echo "[Desktop Entry]
Type=Application
Name[ar]=تمديدات
Name=Extensions
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=org.gnome.Extensions
Comment[ar]=اضبط امتدادات صدفة جنوم
Comment=Configure GNOME Shell Extensions
Exec=/usr/bin/gnome-extensions-app --gapplication-service
DBusActivatable=true
Categories=GNOME;GTK;Utility;
OnlyShowIn=GNOME;" | tee -a /usr/share/applications/org.gnome.Extensions.desktop

rm -rf /usr/share/applications/org.gnome.TextEditor.desktop
touch /usr/share/applications/org.gnome.TextEditor.desktop
echo "[Desktop Entry]
Categories=GNOME;GTK;Utility;TextEditor;
Keywords=write;notepad;
DBusActivatable=true
Exec=gnome-text-editor %U
MimeType=text/plain;
Name[ar]=تحرير النص
Name=Text Editor
GenericName=Text Editor
Comment=View and edit text files
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=org.gnome.TextEditor
StartupNotify=true
Terminal=false
Type=Application
X-Flatpak-RenamedFrom=gnome-text-editor" | tee -a /usr/share/applications/org.gnome.TextEditor.desktop
