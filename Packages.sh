#!/bin/bash
echo "Installing Dependencies."
apt-get install wget --no-install-recommends -y

echo "Installing Base Packages. Without recommends"
apt-get install vlc drawing liferea git gnupg lsb-release apt-transport-https ca-certificates clamtk apparmor apparmor-profiles apparmor-utils auditd tlp nodejs npm --no-install-recommends -y

echo "Installing base packages with recommedns"
apt-get install build-essential meson irqbalance clamav -y

echo "nice to have"
apt-get install mesa-vulkan-drivers libglx-mesa0:i386 mesa-vulkan-drivers:i386 libgl1-mesa-dri:i386 libgl1-mesa-dri:i386 default-jdk golang -y

echo "lets also get a better terminal emu"
apt-get install gnome-console --no-install-recommends -y

echo "installing wine from its shit repo"
# i fucking hate wine's repo
dpkg --add-architecture i386
sudo mkdir -pm755 /etc/apt/keyrings
wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo gpg --dearmor -o /etc/apt/keyrings/winehq-archive.key -

sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bookworm/winehq-bookworm.sources
apt update
apt-get install winehq-stable -f --allow-downgrades --allow-change-held-packages --install-recommends -y

# So I have removed codium's repo since it seems the offical debian repo is broken, instead we will just download and install the .deb file
# Current Codium Release used, 1.94.2.24286
wget https://github.com/VSCodium/vscodium/releases/download/1.96.4.25026/codium_1.96.4.25026_amd64.deb
apt install ./codium_1.96.4.25026_amd64.deb -y

# if building for arm devices, comment out the above code or delete and uncomment whats under.
#wget https://github.com/VSCodium/vscodium/releases/download/1.94.2.24286/codium_1.94.2.24286_arm64.deb
#apt install ./codium_1.94.2.24286_arm64.deb -y

echo "I'm Really Happy that's done with. Gonna take a 6 Month long break 2 times this year and the one after it. Okay? Goodbye UwU."