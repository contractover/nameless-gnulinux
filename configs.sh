#!/bin/bash
echo "Applying config files"
#/etc/os-release
rm -rf /etc/os-release
touch /etc/os-release
echo 'PRETTY_NAME="Nabatean GNU+Linux 2.0 Aqaba"
NAME="Nabatean GNU+Linux"
VERSION_ID="2.0"
VERSION="2.0 (Aqaba)"
VERSION_CODENAME=Aqaba
ID=nabatean
HOME_URL="https://codeberg.org/contractover/Nabatean-gnulinux"
SUPPORT_URL="https://codeberg.org/contractover/Nabatean-gnulinux"
BUG_REPORT_URL="https://codeberg.org/contractover/Nabatean-gnulinux"
' | tee -a /etc/os-release
dpkg -S os-release
chmod a-w /etc/os-release
chmod 444 /etc/os-release

#issue
rm -rf /etc/issue
touch /etc/issue
echo "Nabatean GNU/Linux 2.0 \n \l" | tee -a /etc/issue

#issue.net
rm -rf /etc/issue.net
touch /etc/issue.net
echo "Nabatean GNU/Linux 2.0" | tee -a /etc/issue.net

#/etc/dpkg/origins/nabatean
rm -rf /etc/dpkg/origins/nabatean
touch /etc/dpkg/origins/nabatean
echo "Vendor: Nabatean
Vendor-URL: https://codeberg.org/contractover/Nabatean-gnulinux
Bugs: https://codeberg.org/contractover/Nabatean-gnulinux" | tee -a /etc/dpkg/origins/nabatean

#/usr/share/base-files/motd
rm -rf /usr/share/base-files/motd
touch /usr/share/base-files/motd
echo "Nabatean GNU+Linux is Free Software under the GNU-GPL3.0 or later
You can get a copy of the Source Code here https://codeberg.org/contractover/Nabatean-gnulinux

if you are a user with no clue of whats hapanning here, you are in whats known as a TTY. Just restart your computer and you should be okay. if not, run this command.
'systemctl start gdm.service' or if that didn't work run 'sudo apt update && sudo apt-get install gnome-core gdm3 -y'
if nothing works please contact your system admin or contact us for help." | tee -a /usr/share/base-files/motd

#/usr/share/applications/install-debian.desktop
rm -rf /usr/share/applications/install-debian.desktop
touch /usr/share/applications/install-debian.desktop
echo "[Desktop Entry]
Type=Application
Version=1.0
Name=Install Nabatean
GenericName=Calamares Installer
Exec=install-debian
Comment=Calamares — Installer for Nabatean Live
Keywords=calamares;system;install;nabatean;installer
Icon=calamares
Terminal=false
Categories=Qt;System;
StartupWMClass=calamares
StartupNotify=True

Name[ar]=تثبيت الأنباط 
Icon[ar]=calamares
GenericName[ar]=مثبت النظام" | tee -a /usr/share/applications/install-debian.desktop

#/etc/calamares/settings.conf
rm -rf /etc/calamares/settings.conf
touch /etc/calamares/settings.conf
echo "modules-search: [ local, /usr/lib/calamares/modules ]

oem-setup: true

disable-cancel: false

disable-cancel-during-exec: false

quit-at-end: false
 
sequence:
# Phase 1 - prepare.
# View modules are shown as UI pages, jobs from job modules
# are executed immediately in the background.
# Jobs should be executed sparingly (if at all) in this phase.
- show:
  - welcome
  - locale
  - keyboard
  - partition
  - summary
 
# Phase 2 - install.
# View modules are not shown. Only the view modules shown
# in the previous phase are allowed, their names should be
# added here as placeholders to specify the order in which
# view module jobs should be enqueued. Job modules are
# also allowed.
- exec:
  - partition
  - mount
  - unpackfs
  - dpkg-unsafe-io
  - sources-media
  - machineid
  - fstab
  - locale
#  - users
  - keyboard
  - localecfg
  - displaymanager
  - networkcfg
  - hwclock
  - services-systemd
  - bootloader-config
  - grubcfg
  - bootloader
  - packages
  - luksbootkeyfile
  - plymouthcfg
  - initramfscfg
  - initramfs
  - dpkg-unsafe-io-undo
  - sources-media-unmount
  - sources-final
  - umount
 
# Phase 3 - postinstall.
# View modules are shown as UI pages, jobs from job modules are
# executed immediately in the background.
# Jobs should be executed sparingly (if at all) in this phase.
- show:
  - finished
 

branding: debian

prompt-install: false

dont-chroot: false" | tee -a /etc/calamares/settings.conf

