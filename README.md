[![ReadMeSupportPalestine](https://raw.githubusercontent.com/Safouene1/support-palestine-banner/master/banner-support.svg)](https://techforpalestine.org/learn-more)

# Nabatean GNU+Linux
Nabatean is a Distro that aims to be something that beginners and advanced GNU+Linux users can use while maintaining great stability and having the very best Arabic support out of any distro that exists.

![ss](https://i.imgur.com/rwCtr3d.png)

# Releases:
* V1.9 (Aretas) (Current-Stable) https://codeberg.org/contractover/Nabatean-gnulinux/src/branch/main/1.9.md
* V1.8 (Rabbel)(Old-Stable) https://mega.nz/file/5wkRmACa#8R_gmeydWYk2q3GE02kfwidQeBhfWc3XsjoEyehoxpw
* Legacy releases https://archive.org/details/@contractover

# License:
* This Distro only exists under the GNU GENERAL PUBLIC LICENSE V3.0 (GPL3.0) https://www.gnu.org/licenses/gpl-3.0.en.html
* I do NOT use Cuck License's (BSD,MIT,XORG,etc)

# Minimum System Requirements:
* AMD64/Intelx86_64 Based CPU with one core at 1.0GHZ
* 2D&3D Accelerated GPU with 64MB vram.(AMD&Intel Prefered, Nvidia will work though.)
* 2GB RAM.
* 25GB of Storage (SSD,HDD,SD,EMMC)
* UEFI/EFI or BIOS(Note: Systems with a 32bit EFI/BIOS but a 64bit CPU won't work.)
* NOTE: Pretty much any computer from 2006 and newer will work fine with this distro.

# Documentation: 
* https://codeberg.org/contractover/Nabatean-Documentation

# Related Repositories:
* Main Distro Source Code Repo: https://codeberg.org/contractover/Nabatean-gnulinux
* Installer Repo: https://codeberg.org/contractover/Nabatean-Calamares-Installer-Theme
* Translations Repo: https://codeberg.org/contractover/Nabatean-Translations
* Wallpaper: https://codeberg.org/contractover/Nabatean-Wallpaper

# FAQ:
 * What makes Nabatean interesting?
 
Nabatean achieves it's goals of being the distro with the very best Arabic support by providing an experience that allows one to install&setup as well as exclusively use the system fully in Arabic while having none of the classic RTL issues. It also Achives beginner friendliness by bundling tools and other software they may want/need, such as WINE. And the user-land of Nabatean is 100% Free software; although the distro ships a collection of non-free proprietary firmware and drivers, it's only to get hardware that the average user may have to work properly, and this only exists for that. The main software repo as well as preinstalled user-land software is free, and we even filter Flathub to only show users Free Software and block the install of proprietary Software packaged as a flatpak. We also provide newer software than stock Debian without affecting the stability or making a Frankendebian. Last but not least, the Gnome Desktop that ships with Nabatean is less bloated and far cleaner than stock Debian or Fedora.

 * Why did you make this distro?

I originally started the distro as a learning experience, but after a while I realized I could make something really nice and that there are no actively maintained arab distros (Ojuba,Arabix,Arabian,Sabily,Bintoo, and Resala are long dead). After I made a question on r/jordan and saw there were people who would be interested, I opened this repo.

# Plans&Goals:
* Continue making the distro better.
* Take legitimate feedback.
* Make r/linux cope and seeth cuz we made another distro. Cry about it fucker.
* Continue to give a better GNOME Expirince than Stock Debian or Fedora. (What the fuck DGM you guys fuckup gnome so badly yall should be ashamed.)
* Conquer Jordan and Kick Micro$ft out.
* RISC-V later 
* README.md in Arabic 2077

### Yotsuba gives you a 4 leaf clover for good luck.
![Yotsuba giving you a 4 leaf clover for good luck.](https://i.imgur.com/e3MZ5zy.png)

