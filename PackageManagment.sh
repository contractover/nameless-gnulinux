#!/bin/bash
echo "Prep work......."
apt update
apt-get upgrade -y
apt autoremove -y
echo "Flatpacks are great."
apt install flatpak -y
apt install gnome-software-plugin-flatpak -y
flatpak remote-add --if-not-exists --system flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists --system appcenter https://flatpak.elementary.io/repo.flatpakrepo
flatpak remote-add --if-not-exists --system fedora oci+https://registry.fedoraproject.org
flatpak remote-add --if-not-exists gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
flatpak remote-add --if-not-exists --system PureOS https://store.puri.sm/repo/stable/pureos.flatpakrepo
flatpak remote-add --if-not-exists kde-runtime-nightly https://cdn.kde.org/flatpak/kde-runtime-nightly/kde-runtime-nightly.flatpakrepo
flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
echo "BTFO Nonfree Proprietary Flatpaks! This is a FREEEEEEEEEEE Zone."
flatpak remote-modify --subset=floss flathub fedora appcenter gnome-nightly PureOS kde-runtime-nightly flathub-beta
echo "Done :)"
